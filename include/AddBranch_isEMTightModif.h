#include <TString.h>
#include <TFile.h>
#include <TTree.h>
#include <TBranch.h>
#include <TMath.h>

#include "Config.h"
#include "egammaPIDdefs.h"

using namespace HG;

enum Sample{sherpa_gg, sherpa_gj, pythia_gg, pythia_gj};

unsigned int calocuts_photonsNonConverted( Config conf, int conv,
					   // eta position in second sampling
					   float eta2,
					   // transverse energy in calorimeter 
					   double et,
					   // hadronic leakage ratios
					   float Rhad1,
					   float Rhad,
					   // E(7*7) in 2nd sampling
					   float e277,
					   // ratios
					   float Reta,
					   float Rphi,
					   // shower width in 2nd sampling
					   float weta2c,
					   // fraction of energy reconstructed in strips
					   float f1,
					   // (Emax1-Emax2)/(Emax1+Emax2)
					   float Eratio,
					   // difference of energy between max and min
					   float DeltaE,
					   // parametrization of E(2nd max)
					   //float deltaemax2,
					   // shower width in 3 strips in 1st sampling
					   float weta1c,
					   // total shower width in strips
					   float wtot,
					   // E(+/-3)-E(+/-1)/E(+/-1)
					   float fracm,
					   // fraction of energy reconstructed in the 3rd sampling
					   float f3=-999,
					   unsigned int iflag=0);

void AddBranch_isEMTightModif(TTree* t);
void AddBranch_isEMTightModif(TFile* f, TString tname);

void AddBranch_isEMTightModif(TString fname);
void AddBranch_isEMTightModif(Sample);
