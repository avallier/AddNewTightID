Macro to add new photon ID variables in an MxAOD (h012)

1. compile wiht: root Init.C
2. Once in root type AddBranch_isEMTightModif(<MxAODfilename>)
   it will add the variables isEMTight_byHand, isEMTight_tuned, isTight_byHand
   and isTight_tuned